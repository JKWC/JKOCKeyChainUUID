//
//  JKKeyChainUUID.h
//  JKKeychain
//
//  Created by 王冲 on 2018/2/11.
//  Copyright © 2018年 希爱欧科技有限公司. All rights reserved.

#import <Foundation/Foundation.h>

@interface JKKeyChainUUID : NSObject
/**
 *  先从内存中获取uuid，如果没有再从钥匙串中获取，如果还没有就生成一个新的uuid，并保存到钥匙串中供以后使用
 *
 *  @return 设备唯一标识码
 */
- (NSString *)readUDID;
/**
 *  从Keychain中删除唯一设备吗
 */
- (void)deleteUUID;

@end

