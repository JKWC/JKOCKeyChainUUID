//
//  ViewController.m
//  JKOCKeyChainUUID
//
//  Created by 王冲 on 2018/2/12.
//  Copyright © 2018年 希爱欧科技有限公司. All rights reserved.
//

#import "ViewController.h"
#import "JKKeyChainUUID.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
       KeychainItemWrapper *itemWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"JK" accessGroup:nil];
       [itemWrapper setObject:@"JKapp" forKey:(__bridge_transfer id)kSecAttrService];
       [itemWrapper setObject:@"JKapp" forKey:(__bridge_transfer id)kSecAttrAccount];
       [itemWrapper setObject:[self getUUID] forKey:(__bridge_transfer id)kSecValueData];
       [self delete:KEY_IN_KEYCHAIN];
    */
    
    JKKeyChainUUID *keychain = [[JKKeyChainUUID alloc] init];
    NSString *keychainValue = [keychain readUDID];
    NSLog(@"keychainValue = %@",keychainValue);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
