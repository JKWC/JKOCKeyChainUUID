//
//  AppDelegate.h
//  JKOCKeyChainUUID
//
//  Created by 王冲 on 2018/2/12.
//  Copyright © 2018年 希爱欧科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

